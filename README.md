# Deps:

```
wofi 
terminus-font-ttf 
terminus-font
swaylock-effects-git
pipewire
pipewire-pulse
wireplumber
pulsemixer (or pavucontrol)
nomacs
nemo
mpv
lightdm
libappindicator
swayidle
swaybg
wl-copy
wl-clipboard
xdg-desktop-portal-wlr
wf-recorder
mako
alacritty
alacritty-themes
grim
slurp
nerd-fonts
```

Neovim:
```
git clone --depth=1 https://github.com/savq/paq-nvim.git \
    "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/pack/paqs/start/paq-nvim
```

```
paru -S emacs-gcc-wayland-devel-bin
```
