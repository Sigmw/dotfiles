local hi = require("functions.highlight")
local colors = require("colors")

hi("DashboardHeader", { guifg = "#18accc" })
hi("DashboardFooter", { guifg = "#18accc"})
hi("DashboardCenter", { guifg = colors.blue })

hi("DashboardShortCut", { guifg = colors.blue })
hi("Normal", { guibg = "#1b1b1a" })
hi("EndOfBuffer", { guibg = "#1b1b1a" })

vim.g.dashboard_custom_header = {
'⣿⣿⣿⣿⣿⣿⡿⣟⠻⠯⠭⠉⠛⠋⠉⠉⠛⠻⢿⣿⣿⣿⣿⣿⣿',
'⣿⣿⣿⣿⡽⠚⠉⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⣀⠀⠈⠙⢿⣿⣿⣿',
'⣿⣿⠏⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣾⣿⣿⣿⣷⣦⡀⠶⣿⣿⣿',
'⣿⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⡆⢻⣿⣿',
'⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣤⣻⣿⣯⣤⣹⣿',
'⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⢿⣿⡇⠀⣿⢟⣿⡀⠟⢹⣿',
'⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣷⣤⣤⣼⣿⣿⡄⢹⣿',
'⣷⠀⠀⠀⠶⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣿⠛⠉⠈⢻',
'⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠋⠛⠛⠛⠀⠀⣤⣾',
'⣿⣿⣿⣷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠉⠉⠉⠛⠁⣰⣿⣿',
'⣿⣿⣿⣿⣿⣷⣦⣤⣤⣤⣤⣄⣀⣀⣀⣀⣀⣠⣤⣤⣤⣾⣿⣿⣿',
}

vim.g.dashboard_custom_footer = {
	"sudo ln -s /etc/runit/sv/bulldog /run/runit/service", 
}

vim.g.dashboard_custom_section = {
	a = { description = { " New File         " }, command = "DashboardNewFile" },
	b = { description = { " Open File Manager" }, command = "NvimTreeOpen" },
	c = { description = { " Close Neovim     " }, command = "qa" },
}
